package rs.laniteo.elasticseach.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rs.laniteo.elasticseach.ElasticsearchApp;
import rs.laniteo.elasticseach.model.Comment;
import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.repository.CommentRepository;
import rs.laniteo.elasticseach.repository.MovieRepository;
import rs.laniteo.elasticseach.service.CommentService;
import rs.laniteo.elasticseach.service.MovieService;
import rs.laniteo.elasticseach.service.impl.CommentServiceImpl;
import rs.laniteo.elasticseach.service.impl.MovieServiceImpl;
import rs.laniteo.elasticseach.support.CommentDTOToComment;
import rs.laniteo.elasticseach.support.Validation;
import rs.laniteo.elasticseach.web.controller.CommentController;
import rs.laniteo.elasticseach.web.dto.CommentDTO;
/*//@RunWith(SpringRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = { ElasticsearchApp.class })
*///@WebMvcTest(CommentController.class)
//@ContextConfiguration
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElasticsearchApp.class)
public class ElasticSearchTest {
	//@Autowired
	private MockMvc mvc;
	@Autowired
	WebApplicationContext webApplicationContext;
	
	
	/*	private Node node;
	@TestConfiguration
	@EnableElasticsearchRepositories(basePackages="rs.laniteo.elasticseach.repository")
    static class EmployeeServiceImplTestContextConfiguration {
  
        @Bean
        public CommentService commentService() {
            return new CommentServiceImpl();
        }
        @Bean
        public MovieService movieService() {
            return new MovieServiceImpl();
        }
    }
 
    @MockBean
    private CommentRepository commentRepository;
    @Autowired
    private CommentService commentService;
    
    @MockBean
    private MovieRepository movieRepository;
    @Autowired
    private MovieService movieService;
    @MockBean
    Validation validation;
    @MockBean
    CommentDTOToComment toComment;*/
	/*@Autowired
    private ElasticsearchTemplate esTemplate;*/
	
	@Autowired
	private CommentService cs;
	@Autowired
	private MovieService ms;
	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		/*Settings.Builder elasticsearchSettings = Settings.settingsBuilder()
                .put("http.enabled", "true")
                .put("cluster.name", "testCluster")
                .put("path.home", "elasticsearch-test")
                .put("elasticsearch.host","localhost")
                .put("elasticsearch.port", "9300");

        node = NodeBuilder.nodeBuilder()
                .local(true)
                .settings(elasticsearchSettings.build())
                .node();*/
        //DeleteIndexRequest request = new DeleteIndexRequest("testset");
      // node.client().admin().indices().prepareDelete("testset").execute();
       //node.client().admin().indices().prepareCreate("testset").get();
		
	}

	@Test
	public void testPost() throws Exception {
		CommentDTO commentDto=new CommentDTO();
		commentDto.setContent("Comment content for testing.");
		commentDto.setMovieId("1");
		String jsonComment=mapToJson(commentDto);
		MvcResult mvcResult=mvc.perform(MockMvcRequestBuilders.post("/comments").contentType(MediaType.APPLICATION_JSON).content(jsonComment)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);
		Comment savedComment=mapFromJson(mvcResult.getResponse().getContentAsString(), Comment.class);
		assertNotNull(savedComment.getId());
		assertEquals("No title", savedComment.getTitle());
		
		//trying to post comment with empty content
		commentDto.setContent("   ");
		jsonComment=mapToJson(commentDto);
		mvcResult=mvc.perform(MockMvcRequestBuilders.post("/comments").contentType(MediaType.APPLICATION_JSON).content(jsonComment)).andReturn();
		status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		assertEquals("MovieId and content can not be empty.", mvcResult.getResponse().getContentAsString());
		//trying to save comment with passed id from non-existent movie
		CommentDTO commentDtoWithNonExistentMovieId=new CommentDTO();
		commentDtoWithNonExistentMovieId.setContent("Comment content for testing.");
		commentDtoWithNonExistentMovieId.setMovieId("non-existent movie id");
		jsonComment=mapToJson(commentDtoWithNonExistentMovieId);
		try {
			mvcResult=mvc.perform(MockMvcRequestBuilders.post("/comments").contentType(MediaType.APPLICATION_JSON).content(jsonComment)).andReturn();
		} catch (Exception e) {
			assertEquals("Can not set non-existent movie.", e.getCause().getMessage());
		}
		
		//
	}
	
	@Test
	public void testDelete() throws Exception{
		//saving one instance of comment and then deleting it
		Comment comment=new Comment();
		comment.setContent("Comment content for testing.");
		cs.save(comment);
		MvcResult mvcResult=mvc.perform(MockMvcRequestBuilders.delete("/comments/"+comment.getId())).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(204, status);
		//trying to delete non-existent instance of comment
		try {
			mvc.perform(MockMvcRequestBuilders.delete("/comments/1000")).andReturn();
		} catch (Exception e) {
			assertEquals("Can not delete non-existent comment.", e.getCause().getMessage());
		}
		
	}

	@Test
	public void testGet() throws Exception{
		//search all
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/comments/all").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   int status = mvcResult.getResponse().getStatus();
	   assertEquals(200, status);
	   String content = mvcResult.getResponse().getContentAsString();
	   Comment[] commentList = mapFromJson(content, Comment[].class);
	   assertTrue(commentList.length > 0);
	   //search by id of non-existent comment instance
	   mvcResult = mvc.perform(MockMvcRequestBuilders.get("/comments/1000").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   status = mvcResult.getResponse().getStatus();
	   assertEquals(404, status);
	   
	   //search by movie title
	   mvcResult = mvc.perform(MockMvcRequestBuilders.get("/comments/movies").param("movie_title", "MOVIE2").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   status = mvcResult.getResponse().getStatus();
	   assertEquals(200, status);
	}
	
	@Test
	public void testPut() throws Exception{
		//update existing comment
		CommentDTO commentDto=new CommentDTO();
		commentDto.setContent("Comment content for testing.");
		commentDto.setId("5");
		String jsonComment=mapToJson(commentDto);
		MvcResult mvcResult=mvc.perform(MockMvcRequestBuilders.put("/comments/"+commentDto.getId()).contentType(MediaType.APPLICATION_JSON).param("id", commentDto.getId()).content(jsonComment)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		Comment updatedComment=mapFromJson(mvcResult.getResponse().getContentAsString(), Comment.class);
		assertEquals(commentDto.getContent(), updatedComment.getContent());
		
		//updating non-existent content
		commentDto.setId("non-existent id");
		jsonComment=mapToJson(commentDto);
		try {
			mvcResult=mvc.perform(MockMvcRequestBuilders.put("/comments/"+commentDto.getId()).contentType(MediaType.APPLICATION_JSON).param("id", commentDto.getId()).content(jsonComment)).andReturn();
		} catch (Exception e) {
			assertEquals("Can not update non-existent comment.", e.getCause().getMessage());
		}
	}
	
	@Test
	public void testMovies() throws Exception{
		//search all
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/movies/all").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   int status = mvcResult.getResponse().getStatus();
	   assertEquals(200, status);
	   String content = mvcResult.getResponse().getContentAsString();
	   Movie[] movieList = mapFromJson(content, Movie[].class);
	   assertTrue(movieList.length > 0);
		   
		//search by title
	   mvcResult = mvc.perform(MockMvcRequestBuilders.get("/movies").param("title", "non-existent movie title").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   status = mvcResult.getResponse().getStatus();
	   assertEquals(404, status);
	}
	 private <T> T mapFromJson(String json, Class<T> clazz)
		      throws JsonParseException, JsonMappingException, IOException {
	      ObjectMapper objectMapper = new ObjectMapper();
	      return objectMapper.readValue(json, clazz);
	 }
	 private String mapToJson(Object obj) throws JsonProcessingException {
	      ObjectMapper objectMapper = new ObjectMapper();
	      return objectMapper.writeValueAsString(obj);
	 }

}
