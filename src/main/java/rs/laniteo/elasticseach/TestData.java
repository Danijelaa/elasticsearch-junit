package rs.laniteo.elasticseach;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.model.User;
import rs.laniteo.elasticseach.model.Comment;
import rs.laniteo.elasticseach.service.MovieService;
import rs.laniteo.elasticseach.service.CommentService;

@Component
public class TestData {

	@Autowired
	private MovieService ms;
	@Autowired
	private CommentService cs;
	
	private boolean existsInList(List<String> list, String item) {
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).equals(item)) {
				return true;
			}
		}
		return false;
	}
	
	@PostConstruct
	public void init() {
		List<Integer> commentIds=new ArrayList<Integer>();
		commentIds.add(1);
		for(int i=1; i<=5; i++) {
			Movie movie=new Movie();
			movie.setId(Integer.toString(i));
			movie.setTitle("MOVIE"+i);
			//List<String> actorsNames=new ArrayList<String>();
			for(int j=1; j<=5; j++) {
				int n=(int) (Math.random()*((10 - 1) + 1) + 1);
				boolean exists=existsInList(movie.getActors(), "actor"+n);
				if(!exists) {
					movie.getActors().add("actor"+n);
				}
			}
			ms.save(movie);
			
			for(int j=1; j<=6; j++) {
				int commentId=commentIds.get(commentIds.size()-1);
				
				Comment comment=new Comment();
				comment.setContent("Content"+commentId);
				comment.setDate(new Date());
				comment.setId(Integer.toString(commentId));
				comment.setMovie(movie);
				comment.setTitle("Comment title "+commentId);
				commentIds.add(++commentId);
				User user=new User();
				int n=(int) (Math.random()*((10 - 1) + 1) + 1);
				user.setUsername("username"+n);
				comment.setUser(user);
				cs.save(comment);
			}
		}
		
		
	}
}
