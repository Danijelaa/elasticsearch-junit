package rs.laniteo.elasticseach.service;

import java.util.List;

import rs.laniteo.elasticseach.model.Movie;

public interface MovieService {

	Movie save(Movie movie);
	void delete(Movie movie);
	Movie findOne(String id);
	Iterable<Movie> findAll();
	Movie findByTitle(String title);
	Iterable<Movie> findByActors(List<String> actors);
}
