package rs.laniteo.elasticseach.service;

import rs.laniteo.elasticseach.model.Comment;

public interface CommentService {

	Iterable<Comment> findAll();
	void save(Comment comment);
	Comment findOne(String id);
	void delete(String id);
	Iterable<Comment> findByMovieId(String id);
	Iterable<Comment> findByMovieTitle(String title);
	Iterable<Comment> findByUser(String username);
	Iterable<Comment> findByMovieAndUser(String title, String username);
}
