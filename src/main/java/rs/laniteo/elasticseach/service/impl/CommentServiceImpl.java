package rs.laniteo.elasticseach.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.laniteo.elasticseach.model.Comment;
import rs.laniteo.elasticseach.repository.CommentRepository;
import rs.laniteo.elasticseach.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentRepository cr;
	
	@Override
	public Iterable<Comment> findAll() {
		return cr.findAll();
	}

	@Override
	public void save(Comment review) {
		cr.save(review);
	}

	@Override
	public Iterable<Comment> findByMovieId(String id) {
		return cr.findByMovieId(id);
	}

	@Override
	public Iterable<Comment> findByMovieTitle(String title) {
		return cr.findByMovieTitle(title);
	}

	@Override
	public Iterable<Comment> findByUser(String username) {
		return cr.findByUserUsername(username);
	}

	@Override
	public Iterable<Comment> findByMovieAndUser(String title, String username) {
		return cr.findByMovieTitleAndUserUsername(title, username);
	}

	@Override
	public Comment findOne(String id) {
		return cr.findOne(id);
	}

	@Override
	public void delete(String id) {
		Comment comment=cr.findOne(id);
		if(comment==null) {
			throw new IllegalArgumentException("Can not delete non-existent comment.");
		}
		cr.delete(id);
	}

	

}
