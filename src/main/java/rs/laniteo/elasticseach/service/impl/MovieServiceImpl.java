package rs.laniteo.elasticseach.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.repository.MovieRepository;
import rs.laniteo.elasticseach.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService{
	@Autowired
	private MovieRepository mr;

	@Override
	public Movie save(Movie movie) {
		return mr.save(movie);
	}

	@Override
	public void delete(Movie movie) {
		mr.delete(movie);
	}

	@Override
	public Movie findOne(String id) {
		return mr.findOne(id);
	}

	@Override
	public Iterable<Movie> findAll() {
		return mr.findAll();
	}

	@Override
	public Movie findByTitle(String title) {
		return mr.findByTitle(title);
	}

	@Override
	public Iterable<Movie> findByActors(List<String> actors) {
		return mr.findByActors(actors);
	}

}
