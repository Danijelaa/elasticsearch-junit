package rs.laniteo.elasticseach.web.dto;

import java.util.Date;

import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.model.User;

public class CommentDTO {
	
	private String id;
	private String title;
	private String movieId;
	private String content;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
