package rs.laniteo.elasticseach.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.service.MovieService;

@RestController
@RequestMapping(value="/movies")
public class MovieController {

	@Autowired
	private MovieService ms;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	ResponseEntity<List<Movie>> getAll(){
		List<Movie> books=new ArrayList<Movie>();
		Iterator<Movie> it=ms.findAll().iterator();
		while(it.hasNext()) {
			books.add(it.next());
		}
		return new ResponseEntity<List<Movie>>(books, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<?> getById(@PathVariable String id){
		Movie movie=ms.findOne(id);
		if(movie==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Movie>(movie, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<?> getByTitle(@RequestParam String title){
		Movie movie=ms.findByTitle(title);
		if(movie==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Movie>(movie, HttpStatus.OK);
	}
	
/*	@RequestMapping(method=RequestMethod.GET, value="/actors")
	ResponseEntity<List<Movie>> getByActors(@RequestParam List<String> actors_names){
		//System.out.println(actors_names.get(0));
		List<Movie> movies=new ArrayList<Movie>();
		Iterator<Movie> it=ms.findByActors(actors_names).iterator();
		while(it.hasNext()) {
			movies.add(it.next());
		}
		return new ResponseEntity<List<Movie>>(movies, HttpStatus.OK);
	}*/
	
	
	
}
