package rs.laniteo.elasticseach.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.elasticseach.model.Comment;
import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.service.CommentService;
import rs.laniteo.elasticseach.service.MovieService;
import rs.laniteo.elasticseach.support.CommentDTOToComment;
import rs.laniteo.elasticseach.support.Validation;
import rs.laniteo.elasticseach.web.dto.CommentDTO;

@RestController
@RequestMapping(value="/comments")
public class CommentController {

	@Autowired
	private CommentService cs;
	@Autowired
	private MovieService ms;
	@Autowired
	private Validation validation;
	@Autowired
	private CommentDTOToComment toComment;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	ResponseEntity<List<Comment>> getAll(){
		List<Comment> reviews=new ArrayList<Comment>();
		Iterator<Comment> it=cs.findAll().iterator();
		while(it.hasNext()) {
			reviews.add(it.next());
		}
		return new ResponseEntity<List<Comment>>(reviews, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	ResponseEntity<Comment> getOne(@PathVariable String id){
		Comment comment=cs.findOne(id);
		if(comment==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Comment>(comment, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/movies")
	ResponseEntity<?> getByMovie(@RequestParam String movie_title){
		Movie movie=ms.findByTitle(movie_title);
		if(movie==null) {
			return new ResponseEntity<String>("No movie with given title.", HttpStatus.NOT_FOUND);
		}
		List<Comment> reviews=new ArrayList<Comment>();
		Iterator<Comment> it=cs.findByMovieTitle(movie_title).iterator();
		while(it.hasNext()) {
			reviews.add(it.next());
		}
		return new ResponseEntity<List<Comment>>(reviews, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/users")
	ResponseEntity<List<Comment>> getByUser(@RequestParam String username){
		
		List<Comment> reviews=new ArrayList<Comment>();
		Iterator<Comment> it=cs.findByUser(username).iterator();
		while(it.hasNext()) {
			reviews.add(it.next());
		}
		return new ResponseEntity<List<Comment>>(reviews, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/movies/users")
	ResponseEntity<?> getByMovieAndUser(@RequestParam String movie_title, @RequestParam String username){
		if(movie_title.trim().equals("")) {
			return getByUser(username);
		}
		if(username.trim().equals("")) {
			return getByMovie(movie_title);
		}
		List<Comment> reviews=new ArrayList<Comment>();
		Iterator<Comment> it=cs.findByMovieAndUser(movie_title, username).iterator();
		while(it.hasNext()) {
			reviews.add(it.next());
		}
		return new ResponseEntity<List<Comment>>(reviews, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<?> addComment(@RequestBody CommentDTO commentDto){
		boolean validated=validation.validatedNewComment(commentDto);
		if(!validated) {
			return new ResponseEntity<String>("MovieId and content can not be empty.", HttpStatus.BAD_REQUEST);
		}
		Comment comment=toComment.convert(commentDto);
		cs.save(comment);
		return new ResponseEntity<Comment>(comment,HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	ResponseEntity<?> updateComment(@RequestBody CommentDTO commentDto, @PathVariable String id){
		if(!id.equals(commentDto.getId())) {
			return new ResponseEntity<String>("Ids do not match.", HttpStatus.BAD_REQUEST);
		}
		boolean validated=validation.validatedCommentForUpdate(commentDto);
		if(!validated) {
			return new ResponseEntity<String>("Content can not be empty.", HttpStatus.BAD_REQUEST);
		}
		Comment comment=toComment.convert(commentDto);
		cs.save(comment);
		return new ResponseEntity<Comment>(comment,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	ResponseEntity<?> deleteOne(@PathVariable String id){
		//Comment comment=cs.findOne(id);
		/*if(comment==null) {
			return new ResponseEntity<String>("Can not delete non-existent comment.", HttpStatus.NOT_FOUND);
		}*/
		cs.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(
					DataIntegrityViolationException e){
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
}
