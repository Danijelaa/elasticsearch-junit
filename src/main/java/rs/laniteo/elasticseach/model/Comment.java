package rs.laniteo.elasticseach.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "moviereviews", type="comments")
public class Comment {

	private static int index=31;
	
	@Id
	private String id;
	private Date date;
	private String title;
	private Movie movie;
	private String content;
	private User user;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String contentn) {
		this.content = contentn;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public static int getIndex() {
		return index;
	}
	public static void setIndex(int index) {
		Comment.index = index;
	}
	
	
}
