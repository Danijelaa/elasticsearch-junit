package rs.laniteo.elasticseach.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticseach.model.Comment;

@Component
public interface CommentRepository extends ElasticsearchRepository<Comment, String>{

	Iterable<Comment> findByMovieId(String id);
	Iterable<Comment> findByMovieTitle(String title);
	Iterable<Comment> findByUserUsername(String title);
	Iterable<Comment> findByMovieTitleAndUserUsername(String title, String username);
}
