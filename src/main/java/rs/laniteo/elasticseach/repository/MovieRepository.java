package rs.laniteo.elasticseach.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticseach.model.Movie;
@Component
public interface MovieRepository extends ElasticsearchRepository<Movie, String>{

	Movie findByTitle(String title);
	List<Movie> findByActors(List<String> actors);
}
