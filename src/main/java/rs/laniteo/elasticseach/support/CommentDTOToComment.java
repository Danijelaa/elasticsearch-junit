package rs.laniteo.elasticseach.support;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticseach.model.Comment;
import rs.laniteo.elasticseach.model.Movie;
import rs.laniteo.elasticseach.model.User;
import rs.laniteo.elasticseach.service.CommentService;
import rs.laniteo.elasticseach.service.MovieService;
import rs.laniteo.elasticseach.web.dto.CommentDTO;
@Component
public class CommentDTOToComment implements Converter<CommentDTO, Comment>{

	@Autowired
	MovieService ms;
	@Autowired
	CommentService cs;
	
	@Override
	public Comment convert(CommentDTO source) {
		//System.out.println(source.getId());
		Comment comment;
		if(source.getId()==null) {
			comment=new Comment();
			int id=Comment.getIndex();
			comment.setId(Integer.toString(id));
			id++;
			Comment.setIndex(id);
			Movie movie=ms.findOne(source.getMovieId());
			if(movie==null) {
				throw new IllegalArgumentException("Can not set non-existent movie.");
			}
			comment.setMovie(movie);
			User user=new User();
			int n=(int) (Math.random()*((10 - 1) + 1) + 1);
			user.setUsername("username"+n);
			comment.setUser(user);
			if(source.getTitle()==null || source.getTitle().trim().equals("")) {
				comment.setTitle("No title");
			}
			else {
				comment.setTitle(source.getTitle());
			}
		}
		else {
			comment=cs.findOne(source.getId());
			if(comment==null){
				throw new IllegalArgumentException("Can not update non-existent comment.");
			}
		}
		comment.setContent(source.getContent());
		comment.setDate(new Date());
		
		return comment;
	}

}
