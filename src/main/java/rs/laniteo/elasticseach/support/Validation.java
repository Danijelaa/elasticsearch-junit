package rs.laniteo.elasticseach.support;

import org.springframework.stereotype.Component;

import rs.laniteo.elasticseach.web.dto.CommentDTO;
@Component
public class Validation {

	public boolean validatedNewComment(CommentDTO commentDto) {
		if(commentDto.getMovieId()==null || commentDto.getMovieId().trim().equals("") 
				|| commentDto.getContent()==null || commentDto.getContent().trim().equals("")) {
			return false;
		}
		return true;
	}
	
	public boolean validatedCommentForUpdate(CommentDTO commentDto) {
		if(commentDto.getContent()==null || commentDto.getContent().trim().equals("")) {
			return false;
		}
		return true;
	}
}
