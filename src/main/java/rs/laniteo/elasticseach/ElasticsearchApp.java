package rs.laniteo.elasticseach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ElasticsearchApp extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ElasticsearchApp.class, args);
	}

}
